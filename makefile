all : remote.app channels.app dad_remote.app

remote.app : remote.c
	gcc remote.c -o remote.app -DMODE_KIDS

channels.app : remote.c
	gcc remote.c -o channels.app -DMODE_ALL

dad_remote.app : remote.c
	gcc remote.c -o dad_remote.app -DMODE_DAD

clean :
	rm *.o *.app *.bak


install :
	sudo cp remote.app /var/www/remotecontrol/
	sudo cp channels.app /var/www/remotecontrol/
	sudo cp dad_remote.app /var/www/remotecontrol/
	sudo cp channel_list.txt /var/www/remotecontrol/
	sudo cp icons/* /var/www/remotecontrol/
	sudo cp startup.jpg /var/www/remotecontrol/
	sudo cp remotecontrol_icon.png /var/www/remotecontrol/
