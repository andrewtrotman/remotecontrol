/*
	REMOTE.C
	--------
	Copyright (c) 2012-2013 Andrew Trotman
	Licensed BSD
*/
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

char *script_name = "exe";
char *query_string = "?";

#define FLOATING 0
#define FIXED 1

/*
	RENDER_KIDS_CHANNELS()
	----------------------
*/
int render_kids_channels(void)
{
printf("<table width=100%%>\n");
printf("<tr>\n");
printf("<td align=center><a href=%s?105><img border=0 width=450 src=disneyjunior.png></a></td>\n", script_name);
printf("<td align=center><a href=%s?100><img border=0 width=450 src=disney.jpg></a></td>\n", script_name);
printf("</tr>\n");
printf("<tr>\n");
printf("<td align=center><a href=%s?104><img border=0 width=450 src=nickjr.jpg></a></td>\n", script_name);
printf("<td align=center><a href=%s?101><img border=0 width=450 src=nickelodeon.jpg></a></td>\n", script_name);
printf("</tr>\n");
printf("<tr>\n");
printf("<td align=center><a href=%s?106><img border=0 width=450 src=kidzone.jpg></a></td>\n", script_name);
printf("<td align=center><a href=%s?102><img border=0 width=450 src=cartoonnetwork.jpg></a></td>\n", script_name);
printf("</tr>\n");
printf("<tr>\n");
printf("<td align=center><a href=%s?9><img border=0 height=154 src=foodtv.png></a></td>\n", script_name);
printf("<td align=center><a href=%s?8><img border=0 width=450 src=livingchannel.png></a></td>\n", script_name);
printf("</tr>\n");
printf("</table>\n");
}

/*
	RENDER_DAD_CHANNELS()
	---------------------
*/
int render_dad_channels(void)
{
printf("<table width=100%%>\n");

/*
	Music Channels
*/
printf("<tr><td colspan=6 style=\"font-size:60pt;\"><center><b>Music Channels</b></center></td></tr>\n");
printf("<tr>\n");
printf("<td align=center><a href=%s?110><img border=0 width=200 src=mtvhits.jpg></a></td>\n", script_name);
printf("<td align=center><a href=%s?111><img border=0 width=200 src=mtvclassic.jpg></a></td>\n", script_name);
printf("<td align=center><a href=%s?112><img border=0 width=200 src=juicetv.png></a></td>\n", script_name);
printf("<td align=center><a href=%s?113><img border=0 width=150 src=j2.png></a></td>\n", script_name);
printf("<td align=center><a href=%s?114><img border=0 width=150 src=c4.png></a></td>\n", script_name);
printf("<td align=center><a href=%s?14><img border=0 width=200 src=mtv.jpg></a></td>\n", script_name);
printf("</tr>\n");

/*
	Kids Channels
*/
printf("<tr><td colspan=6 style=\"font-size:10pt;\">&nbsp;</td></tr>\n");
printf("<tr><td colspan=6 style=\"font-size:60pt;\"><center><b>Kids Channels</b></center></td></tr>\n");
printf("<tr>\n");
printf("<td align=center><a href=%s?105><img border=0 width=200 src=disneyjunior.png></a></td>\n", script_name);
printf("<td align=center><a href=%s?100><img border=0 width=200 src=disney.jpg></a></td>\n", script_name);
printf("<td align=center><a href=%s?104><img border=0 width=200 src=nickjr.jpg></a></td>\n", script_name);
printf("<td align=center><a href=%s?101><img border=0 width=200 src=nickelodeon.jpg></a></td>\n", script_name);
printf("<td align=center><a href=%s?106><img border=0 width=200 src=kidzone.jpg></a></td>\n", script_name);
printf("<td align=center><a href=%s?102><img border=0 width=200 src=cartoonnetwork.jpg></a></td>\n", script_name);
printf("</tr>\n");

/*
	Lifestyle Channels
*/
printf("<tr><td colspan=6 style=\"font-size:10pt;\">&nbsp;</td></tr>\n");
printf("<tr><td colspan=6 style=\"font-size:60pt;\"><center><b>Lifestyle Channels</b></center></td></tr>\n");
printf("<tr>\n");
printf("<td align=center><a href=%s?9><img border=0 width=150 src=foodtv.png></a></td>\n", script_name);
printf("<td align=center><a href=%s?8><img border=0 width=150 src=livingchannel.png></a></td>\n", script_name);
printf("<td align=center><a href=%s?25><img border=0 width=200 src=travelchannel.jpg></a></td>\n", script_name);
printf("<td align=center><a href=%s?26><img border=0 width=200 src=choicetv.jpg></a></td>\n", script_name);
printf("<td align=center><a href=%s?1><img border=0 height-154 src=e!.jpg></a></td>\n", script_name);
printf("<td align=center><a href=%s?17><img border=0 height-154 src=tvnzheartland.jpg></a></td>\n", script_name);
printf("</tr>\n");

/*
	News Channels
*/
printf("<tr><td colspan=6 style=\"font-size:10pt;\">&nbsp;</td></tr>\n");
printf("<tr><td colspan=6 style=\"font-size:60pt;\"><center><b>News Channels</b></center></td></tr>\n");
printf("<tr>\n");
printf("<td align=center><a href=%s?85><img border=0 width=200 src=skynewsnz.jpg></a></td>\n", script_name);
printf("<td align=center><a href=%s?86><img border=0 width=200 src=parliamenttv.jpg></a></td>\n", script_name);
printf("<td align=center><a href=%s?87><img border=0 width=200 src=cnn.jpg></a></td>\n", script_name);
printf("<td align=center><a href=%s?88><img border=0 width=200 src=foxnews.jpg></a></td>\n", script_name);
printf("<td align=center><a href=%s?89><img border=0 width=200 src=bbcworldnews.jpg></a></td>\n", script_name);
printf("<td align=center><a href=%s?90><img border=0 width=200 src=aljazeera.jpg></a></td>\n", script_name);
printf("</tr>\n");
printf("<tr>\n");
printf("<td align=center><a href=%s?91><img border=0 width=200 src=cnbc.jpg></a></td>\n", script_name);
printf("<td align=center><a href=%s?92><img border=0 width=200 src=russiatoday.jpg></a></td>\n", script_name);
printf("<td align=center><a href=%s?310><img border=0 width=200 src=cctvnews.jpg></a></td>\n", script_name);
printf("<td></td>\n");
printf("<td></td>\n");
printf("<td></td>\n");
printf("</tr>\n");

/*
	Documentary Channels
*/
printf("<tr><td colspan=6 style=\"font-size:10pt;\">&nbsp;</td></tr>\n");
printf("<tr><td colspan=6 style=\"font-size:60pt;\"><center><b>Documentary Channels</b></center></td></tr>\n");
printf("<tr>\n");
printf("<td align=center><a href=%s?70><img border=0 width=200 src=discoverychannel.jpg></a></td>\n", script_name);
printf("<td align=center><a href=%s?71><img border=0 width=200 src=crimeinvestigation.jpg></a></td>\n", script_name);
printf("<td align=center><a href=%s?72><img border=0 width=200 src=nationalgeographic.jpg></a></td>\n", script_name);
printf("<td align=center><a href=%s?73><img border=0 width=200 src=historychannel.jpg></a></td>\n", script_name);
printf("<td align=center><a href=%s?74><img border=0 width=200 src=bbcknowledge.jpg></a></td>\n", script_name);
printf("<td align=center><a href=%s?75><img border=0 width=200 src=animalplanet.jpg></a></td>\n", script_name);
printf("</tr>\n");

/*
	Core Channels
*/
printf("<tr><td colspan=6 style=\"font-size:10pt;\">&nbsp;</td></tr>\n");
printf("<tr><td colspan=6 style=\"font-size:60pt;\"><center><b>Core Channels</b></center></td></tr>\n");
printf("<tr>\n");
printf("<td align=center><a href=%s?1><img border=0 width=200 src=tvone.jpg></a></td>\n", script_name);
printf("<td align=center><a href=%s?2><img border=0 width=200 src=tvtwo.jpg></a></td>\n", script_name);
printf("<td align=center><a href=%s?3><img border=0 width=200 src=tv3.jpg></a></td>\n", script_name);
printf("<td align=center><a href=%s?4><img border=0 width=200 src=prime.jpg></a></td>\n", script_name);
printf("<td align=center><a href=%s?7><img border=0 width=200 src=bbc-uktv.jpg></a></td>\n", script_name);
printf("<td align=center><a href=%s?13><img border=0 width=200 src=jones.jpg></a></td>\n", script_name);
printf("</tr>\n");
printf("<tr>\n");
printf("<td align=center><a href=%s?5><img border=0 width=200 src=box.jpg></a></td>\n", script_name);
printf("<td align=center><a href=%s?6><img border=0 width=200 src=vibe.jpg></a></td>\n", script_name);
printf("<td align=center><a href=%s?12><img border=0 width=200 src=four.jpg></a></td>\n", script_name);
printf("<td align=center><a href=%s?15><img border=0 width=200 src=comedycentral.jpg></a></td>\n", script_name);
printf("<td></td>\n");
printf("<td></td>\n");
printf("</tr>\n");

/*
	Maori Channels
*/
printf("<tr><td colspan=6 style=\"font-size:10pt;\">&nbsp;</td></tr>\n");
printf("<tr><td colspan=6 style=\"font-size:60pt;\"><center><b>Maori &amp; Sports Channels</b></center></td></tr>\n");
printf("<tr>\n");
printf("<td align=center><a href=%s?82><img border=0 width=200 src=tereo.jpg></a></td>\n", script_name);
printf("<td align=center><a href=%s?19><img border=0 width=200 src=maoritv.jpg></a></td>\n", script_name);
printf("<td></td>\n");
/*
	Sports channels
*/
printf("<td align=center><a href=%s?62><img border=0 width=200 src=tabtv.jpg></a></td>\n", script_name);
printf("<td align=center><a href=%s?63><img border=0 width=200 src=trackside.jpg></a></td>\n", script_name);
printf("<td></td>\n");
printf("<td></td>\n");
printf("</tr>\n");

/*
	Shopping and other useless channels
*/
printf("<tr><td colspan=6 style=\"font-size:10pt;\">&nbsp;</td></tr>\n");
printf("<tr><td colspan=6 style=\"font-size:60pt;\"><center><b>Shopping Channels</b></center></td></tr>\n");
printf("<tr>\n");
printf("<td align=center><a href=%s?23><img border=0 width=200 src=yesshop.jpg></a></td>\n", script_name);
printf("<td align=center><a href=%s?24><img border=0 width=200 src=tvsnshopping.jpg></a></td>\n", script_name);
printf("<td align=center><a href=%s?18><img border=0 width=200 src=theshoppingchannel.jpg></a></td>\n", script_name);
printf("<td align=center><a href=%s?83><img border=0 width=200 src=facetv.jpg></a></td>\n", script_name);
printf("<td></td>\n");
printf("<td></td>\n");
printf("</tr>\n");
/*
	printf("<tr>\n");
	printf("<td align=center><a href=%s?28><img border=0 width=200 src=preview.jpg></a></td>\n", script_name);
	printf("<td align=center><a href=%s?29><img border=0 width=200 src=upgrade.jpg></a></td>\n", script_name);
	printf("<td></td>\n");
	printf("<td></td>\n");
	printf("</tr>\n");
*/

/*
	Miscelanious Channels
*/
printf("<tr><td colspan=6 style=\"font-size:10pt;\">&nbsp;</td></tr>\n");
printf("<tr><td colspan=6 style=\"font-size:60pt;\"><center><b>Other Channels</b></center></td></tr>\n");
printf("<tr>\n");
printf("<td align=center><a href=%s?200><img border=0 width=200 src=cuetv.jpg></a></td>\n", script_name);
printf("<td align=center><a href=%s?201><img border=0 width=200 src=shinetv.jpg></a></td>\n", script_name);
printf("<td align=center><a href=%s?202><img border=0 width=200 src=daystar.png></a></td>\n", script_name);
printf("<td></td>\n");
printf("<td></td>\n");
printf("<td></td>\n");
printf("</tr>\n");


printf("</table>\n");
}

/*
	RENDER_ALL_CHANNELS()
	----------------------
*/
int render_all_channels(void)
{
FILE *fp;
char buffer[1024];
int channel;

if ((fp = fopen("channel_list.txt", "r")) != NULL)
	while (fgets(buffer, sizeof(buffer), fp) != NULL)
		if ((channel = atoi(buffer)) != 0)
			printf("<a href=%s?%d>%s</a><br>\n", script_name, channel, buffer);
}

/*
	RENDER_GENERICS()
	-----------------
*/
void render_generics(int fixed_or_floating)
{
printf("<br><br><br><br>\n");
if (fixed_or_floating == FLOATING)
	printf("<table width=100%% style=position:absolute;bottom:0;>\n");
else
	printf("<table width=100%%>\n");
printf("<tr>\n");
printf("<td align=middle>\n");

printf("<a href=%s?on><img border=0 height=150 src=generic_on.jpg></a>\n", script_name);
printf("</td><td align=middle>\n");
printf("<a href=%s?off><img border=0 height=150 src=generic_off.jpg></a>\n", script_name);
printf("</td><td align=middle>\n");
printf("<a href=%s?volumedown><img border=0 height=150 src=generic_volumedown.jpg></a>\n", script_name);
printf("</td><td align=middle>\n");
printf("<a href=%s?mute><img border=0 height=150 src=generic_mute.jpg></a>\n", script_name);
printf("</td><td align=middle>\n");
printf("<a href=%s?volumeup><img border=0 height=150 src=generic_volumeup.jpg></a>\n", script_name);

printf("</td>\n");
printf("</tr>\n");
printf("</table>\n");
}

/*
	IRSEND_SET_CHANNEL()
	--------------------
*/
void irsend_set_channel(int channel)
{
char message[1024];

channel = channel % 1000;

system("irsend send_once sky off");
system("irsend send_once sky power");
system("irsend send_once tv on");
system("irsend send_once tv video1");
sprintf(message, "irsend send_once sky %d", channel / 100);
system(message);
sprintf(message, "irsend send_once sky %d", (channel / 10) % 10);
system(message);
sprintf(message, "irsend send_once sky %d", channel % 10);
system(message);
}

/*
	IRSEND_ALL_OFF()
	----------------
*/
void irsend_all_off(void)
{
system("irsend send_once tv off");
system("irsend send_once sky off");
}

/*
	IRSEND_ALL_ON()
	---------------
*/
void irsend_all_on(void)
{
system("irsend send_once tv on");
system("irsend send_once sky off");
system("irsend send_once sky power");
}

/*
	IRSEND_MUTE()
	-------------
*/
void irsend_mute(void)
{
system("irsend send_once tv mute");
}

/*
	IRSEND_TV_VOLUME_UP()
	---------------------
*/
void irsend_tv_volume_up(void)
{
system("irsend send_once tv volumeup");
}

/*
	IRSEND_TV_VOLUME_DOWN()
	-----------------------
*/
void irsend_tv_volume_down(void)
{
system("irsend send_once tv volumedown");
}

/*
	MAIN()
	------
*/
int main(void)
{
char *tmp;

if ((tmp = getenv("SCRIPT_NAME")) != NULL)
	script_name = tmp;

puts("Content-type: text/html\n");

if ((query_string = getenv("QUERY_STRING")) != NULL)
	{
	if (isdigit(*query_string))
		irsend_set_channel(atol(query_string));
	else if (strcmp(query_string, "off") == 0)
		irsend_all_off();
	else if (strcmp(query_string, "on") == 0)
		irsend_all_on();
	else if (strcmp(query_string, "mute") == 0)
		irsend_mute();
	else if (strcmp(query_string, "volumeup") == 0)
		irsend_tv_volume_up();
	else if (strcmp(query_string, "volumedown") == 0)
		irsend_tv_volume_down();
	}

printf("<html>\n");
printf("<head>\n");

/*
	Apple Webapp
*/
puts("<meta name=\"apple-mobile-web-app-capable\" content=\"yes\">");
puts("<meta name=\"apple-mobile-web-app-title\" content=\"ScarbaTV\">");
puts("<meta name=\"apple-mobile-web-app-status-bar-style\" content=\"black\">");
puts("<link href=\"/remotecontrol/startup.jpg\" media=\"(device-width: 320px) and (device-height: 480px) and (-webkit-device-pixel-ratio: 2)\" rel=\"apple-touch-startup-image\">");
puts("<link rel=\"apple-touch-icon\" href=\"remotecontrol_icon.png\">");

/*
	If we're an Apple WebApp then stay in the same window
*/
puts("<script>(function(a,b,c){if(c in b&&b[c]){var d,e=a.location,f=/^(a|html)$/i;a.addEventListener(\"click\",function(a){d=a.target;while(!f.test(d.nodeName))d=d.parentNode;\"href\"in d&&(d.href.indexOf(\"http\")||~d.href.indexOf(e.host))&&(a.preventDefault(),e.href=d.href)},!1)}})(document,window.navigator,\"standalone\")</script>");

printf("<title>Remote Control</title>");
printf("</head>\n");
printf("<body>\n");
#ifdef MODE_KIDS
	render_kids_channels();
	render_generics(FLOATING);
#elif defined(MODE_ALL)
	render_all_channels();
	render_generics(FIXED);
#elif defined(MODE_DAD)
	render_dad_channels();
	render_generics(FIXED);
#endif
printf("</body>\n");
printf("</html>\n");
}
